var app 				= require('express')(),
	server 				= require('http'),
	mango 				= require('mongoose'),
	ejs 					= require('ejs'),
	session 			= require('cookie-session'),
	bodyParser 			= require('body-parser'),
  urlencodedParser 	= bodyParser.urlencoded({ extended: false }),
	io 					= require('socket.io').listen(server),
// Personal modules
	check 				= require('./node_modules/check');
  layout				= require('./node_modules/layout.js');

mango.connect('mongodb://localhost/TowerDefense', function(err){
		if(err) res.send(err);
	});

var userSchema = new mango.Schema({
	login		: String,
	password	: String,
	friends : [String]
});
var User = mango.model('User', userSchema);


app.use(session({secret: 'TDSecret'}))

.use(function (req, res, next) {
	if(typeof(req.session.login) == 'undefined' && typeof(req.session.password) == 'undefined') {
		req.session.login = null;
		req.session.password = null;
	}
	next();
})


// Arrivé à cette page, on peut s'inscrire, se connecter
.get('/', function (req, res) {
	res.sendFile(__dirname + '/index.html');
})

// Arrivé sur la page d'accueil, on peut demander quelqu'un en amis, ou défier un de ses amis en créant une salle (socket) de jeu
.post('/register/', urlencodedParser, function (req, res) {
	

	// Si c'est une inscription
	if(req.body.submit === 'inscription') {
	// Création du doc de l'utilisateur pour la bdd
		// Si l'utilisateur n'existe pas deja	
		User.find({login: req.body.login}, function (err, doc) {
			// S'il y a une erreur on suppose que c'est parceque le login n'est pas dans la base (en realité, ce n'est surement pas l'unique type d'erreur)
			if (err) {
				// Si personne n'est connecté
				if(req.session.login != null && req.session.password != null) {
					var newUser = new User({
						login 		: req.body.login,
						password	: req.body.password
					});
			
					// Sauvegarde du doc dans la bdd
					newUser.save(function (err) {
						if (err) {
							res.send(err);
						}
						else {
							// On stocke l'identifiant dans une session
							req.session.login = req.body.login;
							req.session.password = req.body.password;
				
							// Puis on affiche l'accueil
							layout.showWelcome(req.session.login);
						}
					});
				}
				else {
					// Afficher la partie qui indique qu'un utilisateur est deja connecté
					layout.showMessage('Ce nom d\'utilsateur est deja utilisé');
				}
			}
			else {
				// Afficher le message d'erreur comme quoi quelqu'un est connecté
				layout.showMessage('Un utilisateur est deja connecté');
			}
		});
	}
	else if (typeof(req.body.submit) == 'undefined' && req.body.submit === 'connection') {
		// Si l'association login/password existe
		check.checkUser(User, req.body.login, req.body.password, function (err) {
						if (err) {
							// Affiche la partie qui dit que les identifiants ne sont pas bons
							layout.showMessage('Les identifiants ne sont pas bons');
						}
						else {
							// On stocke l'identifiant dans une session
							req.session.login = req.body.login;
							req.session.password = req.body.password;
							// Puis on affiche l'accueil
							layout.showWelcome(req.session.login);
						}
					});
	}
})

/*
io.sockets.on('connection', function(socket, pseudo) {
	})
})
*/

.listen(8080);